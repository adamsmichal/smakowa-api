<?php

namespace App\Providers;

use App\Repositories\UserCrud\UserCrudRepository;
use App\Repositories\UserCrud\UserCrudRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserCrudRepositoryInterface::class, UserCrudRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
