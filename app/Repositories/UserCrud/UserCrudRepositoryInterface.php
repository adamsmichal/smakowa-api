<?php

namespace App\Repositories\UserCrud;

interface UserCrudRepositoryInterface
{
    public function temporary(): void;
}
