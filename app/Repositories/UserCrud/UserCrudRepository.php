<?php

namespace App\Repositories\UserCrud;

use App\Models\User;
use App\Repositories\CrudEloquent\CrudEloquentRepository;

class UserCrudRepository extends CrudEloquentRepository implements UserCrudRepositoryInterface
{
    protected User $user;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->user = $user;
    }

    public function temporary(): void
    {
        // TODO: Implement temporary() method.
    }
}
