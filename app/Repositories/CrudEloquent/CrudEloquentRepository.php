<?php

namespace App\Repositories\CrudEloquent;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class CrudEloquentRepository implements CrudEloquentRepositoryInterface
{
    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getAll(): Collection
    {
        return $this->model
                    ->all();
    }

    public function getOneById(Int $id): ?Model
    {
        return $this->model
                    ->query()
                    ->find($id);
    }

    public function create(array $params): Model
    {
        return $this->model
                    ->query()
                    ->create($params);
    }

    public function update(Int $id, array $params): void
    {
        $this->model
            ->query()
            ->find($id)
            ->update($params);
    }

    public function delete(Int $id): void
    {
        $this->model
            ->query()
            ->find($id)
            ->delete();
    }
}
