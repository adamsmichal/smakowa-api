<?php

namespace  App\Repositories\CrudEloquent;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface CrudEloquentRepositoryInterface
{
    public function getAll(): Collection;

    public function getOneById(Int $id): ?Model;

    public function create(array $params): Model;

    public function update(Int $id, array $params): void;

    public function delete(Int $id): void;
}
