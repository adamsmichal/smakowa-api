<?php

namespace App\Http\Controllers;

use App\Repositories\UserCrud\UserCrudRepository;
use App\Repositories\UserCrud\UserCrudRepositoryInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected UserCrudRepository $userCrudRepository;

    public function __construct(UserCrudRepositoryInterface $userCrudRepository)
    {
        $this->userCrudRepository = $userCrudRepository;
    }

    public function temporary()
    {
        $this->userCrudRepository->getAll();
    }
}
