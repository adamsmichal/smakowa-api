<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Jak uruchomić

- Wiadomo na początek git clone
- Kopiujemy .env.example i zmieniamy nazwę na .env
- Musimy mieć zainstalowanego dockera
- W konsoli wpisujemy "docker-compose build"
- Wpisujemy w konsoli "docker-compose run app composer install"
- Wpisujemy w konsoli "docker-compose run app php artisan key:generate"
- Po tym wpisujemy "docker-compose up" i sprawdzamy czy wszystko poprawnie wstaje. Jeśli będą jakieś błędy to tam się wypiszą
- Wszystko powinno już działać

## Problemy jakie napotkałem
Możę się wydarzyć tak że docker spina się o db usera że jest to root. Wtedy w .env i docker-compose.yml trzeba zmienić nazwę usera na root1. Zrobić builda, odpalić upa i wtedy wszystko wstanie. Po tym trzeba znów wrócić do root, zbudować dockera i zrobić upa.

## Linki

- Phpmyadmin: http://0.0.0.0:8090/
- Apka: http://0.0.0.0:8009/
